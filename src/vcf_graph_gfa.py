from bitarray import bitarray
from collections import defaultdict, OrderedDict, Callable
from itertools import product, izip
import argparse
import gzip
import logging
import numpy as np
import re
import sys
import os
from time import time

logger = logging.getLogger(__name__)

NODE_HISTORY_SIZE = 100

# NOTE: Quick and dirty version for now!


def profile(f):
    def wrap(*args, **kwargs):
        fname = f.func_name
        argnames = f.func_code.co_varnames[:f.func_code.co_argcount]
        filled_args = ', '.join(
            '%s=%r' % entry
            for entry in zip(argnames, args[:len(argnames)]) + [("args", list(args[len(argnames):]))] + [("kwargs", kwargs)])
        logger.info('Started: {}({})'.format(fname, filled_args))
        starting_time = time()
        output = f(*args, **kwargs)
        logger.info('Ended: Function -> {}, duration {}s'.format(fname,
                                                                 time() - starting_time))
        return output
    return wrap


class LimitedDefaultOrderedDict(OrderedDict):
    def __init__(self, default_factory=None, *args, **kwds):
        if (default_factory is not None and
                not isinstance(default_factory, Callable)):
            raise TypeError('first argument must be callable')

        self.size_limit = kwds.pop("size_limit", None)

        OrderedDict.__init__(self, *args, **kwds)
        self.default_factory = default_factory
        self._check_size_limit()

    def __getitem__(self, key):
        try:
            return OrderedDict.__getitem__(self, key)
        except KeyError:
            return self.__missing__(key)

    def __setitem__(self, key, value):
        OrderedDict.__setitem__(self, key, value)
        self._check_size_limit()

    def _check_size_limit(self):
        if self.size_limit is not None:
            while len(self) > self.size_limit:
                self.popitem(last=False)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)

    def __deepcopy__(self, memo):
        import copy
        return type(self)(self.default_factory,
                          copy.deepcopy(self.items()))

    def __repr__(self):
        return 'OrderedDefaultDict(%s, %s)' % (self.default_factory,
                                               OrderedDict.__repr__(self))


class open_vcf():
    def __init__(self, path, mode):
        self.path = path
        self.mode = mode

    def __enter__(self):
        self.fp = self._vcf_helper(self.path, self.mode)
        return self.fp

    def __exit__(self, type, value, traceback):
        self.fp.close()

    @staticmethod
    def _vcf_helper(file_path, mode):
        file_type = eval_file_type(file_path)
        if file_type:
            if "gz" == file_type:
                return gzip.open(file_path, 'r')
            else:
                raise Exception
        else:
            return open(file_path, 'r')


class Variant(object):
    def __init__(self, variant, n_samples):
        # TODO: Make the genotype optional
        self.chr = variant[0]
        self.pos = int(variant[1])
        self.ref = variant[3]
        self.alt = variant[4].split(',')

        # TODO: Make this cleaner...
        self.genotype = [item for sublist in [map(int, re.split('[: /|]+', i.replace(
            ".", "0"))[0:2]) for i in variant[9].split(None, n_samples - 1)] for item in sublist]

    def seq(self):
        return [self.ref] + self.alt

    def n_variants(self):
        return len(self.alt)

    def is_snp(self):
        return len(self.ref) == 1 and all(len(i) == 1 for i in self.alt)

    def __len__(self):
        raise self.number_of_variants() + 1

    def __str__(self):
        return "{}:{}:{}:{}".format(self.chr, self.pos, self.ref, self.alt)


def eval_file_type(path):
    magic_dict = {
        "\x1f\x8b\x08": "gz",
        "\x42\x5a\x68": "bz2",
        "\x50\x4b\x03\x04": "zip"
    }

    max_len = max(len(x) for x in magic_dict)

    with open(path, 'r') as f:
        file_start = f.read(max_len)
    for magic, filetype in magic_dict.items():
        if file_start.startswith(magic):
            return filetype
    return 0


def check_minshared(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError(
            "Invalid number of genotypes! GENOTYPE_SHARED={}".format(value))
    return ivalue


def check_subset(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError(
            "Invalid number of genotypes! GENOTYPE_SUBSET={}".format(value))
    return ivalue


def skip_header(file_handle, prefix='#'):
    """
    Returning the file handle, the position past the header
    """

    last_pos = file_handle.tell()

    while file_handle.readline().startswith(prefix):
        last_pos = file_handle.tell()

    file_handle.seek(last_pos)


@profile
def read_fasta(path):
    """
    Read a full fasta file into memory,
    TODO: Change this to a generator that reads blocks
          this will need an auxilliary function that translates the relative genome position to an indexing
          within the block (based on block length and block number)
    """

    with open(path, 'r') as in_fasta:
        header = in_fasta.readline()
        return (header, ''.join(in_fasta.read().splitlines()))


def is_cnv(variant):
    return variant[4].startswith('<')


def build_bit(genotype, index):
    return bitarray(tuple(i == index for i in genotype))


def invert_dictionary(dictionary):
    return defaultdict(bitarray, ((v, k) for k in dictionary for v in dictionary[k]))


def pairwise(iterable):
    a = iter(iterable)
    return izip(a, a)


@profile
def gen_ksnp_set(ksnp_path):
    ksnp_set = set([])
    with open(ksnp_path, 'r') as ksnp_in:
        for line in ksnp_in:
            ksnp_set.add(int(line.split(None, 2)[1]))
    return ksnp_set


def gen_sample_index(bitvector):
    sample_indices = []
    for index, i in enumerate(pairwise(bitvector[:-1])):
        if any(i):
            sample_indices.append(str(index))
    if bitvector[-1]:
        sample_indices.append(str(index + 1))
    return sample_indices


def is_min_var(xs, cutoff):
    if cutoff > len(xs):
        raise ValueError

    count = 0
    for i in xs:
        if i:
            count += 1
        if count >= cutoff:
            return True
    return False


@profile
def vcf_graph(input_fasta, input_vcf, output_file, genotype_subset=None, genotype_shared=None, ksnp_path=None, snp_only=False):
    count = 0
    offset_pos = 0
    fasta_pos = 0
    node_id = 0
    pred = []

    node_history = LimitedDefaultOrderedDict(dict)
    node_history.size_limit = NODE_HISTORY_SIZE

    def write_gfa_node(node_id, sample_index, offset="N"):
        out_gfa.write("S\t{0}\t{1}\t*\tORI:Z:{2}\tN:I:{3}\tSTART:i:{4}\n".format(node_id,
                                                                                 node_history[node_id][
                                                                                     'sequence'],
                                                                                 sample_index, len(node_history[node_id][
                                                                                     'sequence']),
                                                                                 offset))

    def write_gfa_node_short(node_id, sample_index):
        out_gfa.write("S\t{0}\t{1}\t*\tORI:Z:{2}\tN:I:{3}\n".format(node_id,
                                                                    node_history[node_id][
                                                                        'sequence'],
                                                                    sample_index, len(node_history[node_id][
                                                                        'sequence'])))

    def write_gfa_edge(edge_list):
        for (u, v) in edge_list:
            out_gfa.write("L\t{}\t+\t{}\t+\t0M\n".format(u, v))

    # Read the reference into memory; TODO: Change to blocks of sequence
    (__, fasta_content) = read_fasta(input_fasta)

    if ksnp_path:
        ksnp_set = gen_ksnp_set(ksnp_path)

    out_gfa = open(output_file, 'w')

    with open_vcf(input_vcf, 'r') as file_vcf:
        skip_header(file_vcf, prefix="##")
        header = next(file_vcf)

        sample_identifiers = [os.path.basename(i) for i in header.split()[9:]]

        if genotype_subset:
            n_sample_identifiers = genotype_subset
        else:
            n_sample_identifiers = len(sample_identifiers)

        sample_identifiers += ["REFERENCE"]

        out_gfa.write("H\tVN:Z:1.0\n")
        out_gfa.write("H\tBUILD:Z:VCF2GRAPHGFA\n")
        out_gfa.write("H\tORI:Z:{0}\n".format(
            ';'.join(sample_identifiers)))

        # Two bits for each sample, and one for the reference
        bit_vector_size = (n_sample_identifiers * 2) + 1

        # consensus bitvector
        reference_bit = bitarray(tuple(np.ones(bit_vector_size, dtype=bool)))
        reference_sample_index = ';'.join(
            map(str, range(len(sample_identifiers))))

        for var in file_vcf:
            var = var.split(None, 9)

            if is_cnv(var):
                continue

            try:
                variant = Variant(var, n_sample_identifiers)
            except ValueError:
                logger.warning("Skipping corrupted variant entry on Chr: {} at pos: {}".format(
                    variant.chr, variant.pos))

            if snp_only:
                # Skip non-SNPs
                if not variant.is_snp():
                    logger.debug("Skipping non-SNP:\t{}".format(variant))
                    continue

            if ksnp_path:
                # Filter out variants not in knsp file
                if variant.pos not in ksnp_set:
                    logger.debug(
                        "Skipping variant not in ksnp:\t{}".format(variant))
                    continue

            # Skip variant entries without sample support (only relevant when downsampling)
            if not any(variant.genotype):
                continue

            if genotype_shared:
                # Skip variants that are not shared by at least 'genotype_shared' samples
                if not is_min_var(variant.genotype, genotype_shared):
                    logger.debug(
                        "Skipping variant with too few shared samples:\t{}".format(variant))
                    continue

            # Create a consensus node
            if (variant.pos - 1) - fasta_pos > 0:
                node_id += 1

                node_history[node_id]['sequence'] = fasta_content[
                    fasta_pos:variant.pos - 1]

                write_gfa_node(node_id, reference_sample_index, offset_pos)
                write_gfa_edge(product(pred, [node_id]))

                # Move fasta cursor after the variant position
                fasta_pos = variant.pos - 1
                offset_pos = variant.pos - 1

                node_history[node_id]['bit'] = reference_bit
                pred = [node_id]

            created_nodes = []

            # Create node entries
            for index, node_seq in enumerate(variant.seq()):
                count += 1
                sample_bit = build_bit(variant.genotype, index)

                # Index 0 is the reference node
                if index == 0:
                    sample_bit += '1'
                else:
                    sample_bit += '0'

                # Skip any alt alleles that have no sample support (only relevant when downsampling)
                if genotype_subset:
                    if not any(sample_bit):
                        continue

                node_id += 1
                created_nodes.append(node_id)

                node_history[node_id]['sequence'] = node_seq
                node_history[node_id]['bit'] = sample_bit

                # Create edges to predecessor(s) defined as the product of
                # pred and node, only allow edges if there is an
                # intersection between source and sink haplotypes
                valid_edges = [(u, v) for (u, v) in product(
                    pred, [node_id]) if any(node_history[u]['bit'] & node_history[v]['bit'])]

                if index == 0:
                    write_gfa_node(node_id, ';'.join(
                        gen_sample_index(sample_bit)), offset_pos)
                else:
                    write_gfa_node_short(node_id, ';'.join(
                        gen_sample_index(sample_bit)))
                write_gfa_edge(valid_edges)

            if fasta_pos:
                fasta_pos += len(variant.ref)

            if offset_pos:
                offset_pos += len(variant.ref)

            # Set new predecessors
            pred = created_nodes

            # Move node_id cursor
            node_id = max(created_nodes)

            if count % 10000 == 0:
                logger.info("Number of variants processed: {}".format(count))

        # Finalize, create sink node
        logger.info("Last variant processed")
        node_id += 1

        node_history[node_id]['sequence'] = fasta_content[fasta_pos:]
        node_history[node_id]['bit'] = reference_bit

        write_gfa_node(node_id, reference_sample_index, offset_pos)
        write_gfa_edge(product(pred, [node_id]))

    out_gfa.close()


def main():
    logging.basicConfig(
        stream=sys.stdout, format='%(asctime)s - %(levelname)s:%(name)s:%(message)s')

    parser = argparse.ArgumentParser(
        description='Parse supplied VCF and reference FASTA into a GFA graph.')
    parser.add_argument('-f', '--fasta', dest='in_fasta',
                        help='Input FASTA', type=str, required=True)
    parser.add_argument('-v', '--vcf', dest='in_vcf',
                        help='Input VCF', type=str, required=True)
    parser.add_argument('-o', '--output', dest='output_file',
                        help='Output filename', type=str, required=True)
    parser.add_argument('-s', '--subset', dest='genotype_subset',
                        help='Take a subset of genotypes encoded in VCF', type=check_subset)
    parser.add_argument('-m', '--minshared', dest='genotype_shared',
                        help='Only include variants that are shared by at least -m # samples', type=check_minshared)
    # NOTE: For now this works only for one specified chromosome
    parser.add_argument('-ksnp', '--ksnp', dest='ksnp_path',
                        help='Only include variants encoded in a .1ksnp file', type=str)
    parser.add_argument('-snp', '--snp', dest='snp_only',
                        help="Only include SNP variants", action='store_true')
    parser.add_argument('-l', dest='log_level', help='Set the logging level',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
    args = parser.parse_args()

    if args.log_level:
        logging.getLogger().setLevel(getattr(logging, args.log_level))

    vcf_graph(args.in_fasta, args.in_vcf,
              args.output_file, args.genotype_subset, args.genotype_shared, args.ksnp_path, args.snp_only)


if __name__ == '__main__':
    main()
